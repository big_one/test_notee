#!/bin/sh
c=" "
pswd=""
title="       Enter password:"
echo "$title"
while read -s -n 1 char
do
	if [[ $char = "" ]]
	then break
	fi
	c="$c"'*'
	pswd=$pswd$char
	clear
	echo "$title"
	echo "$c"
done
echo $pswd > /tmp/query.sh_ret
# ! clear query.sh_ret after reading !
