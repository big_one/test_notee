#!/bin/sh
# This util files:
#	some_dir
#          ├── notee.sh  (main)
#          ├── msg.sh    (helper)
#          ├── psw.sh    (helper)
#          └── query.sh  (helper)

ret=/tmp/query.sh_ret
export MSG="$2"

## Geometry:
## +0+0	upper left corner
## -0+0	upper right corner
## -0-0	lower right corner
## +0-0	lower left corner
# Width:
w=$(echo -e "$MSG" | head -1 | wc -m)
if (($w < 30))
then w=30
fi
# Height: 
h=$(($(echo -e "$MSG" | wc -l) + 2))

case $1 in
	-m) export DELAY=$3
		run_path=$(dirname $(readlink $0))/msg.sh		# for "ln -s ~/bin/ ..." usage
		xfce4-terminal --disable-server --geometry "$w"x"$h"+0-0 --hide-borders -x $run_path ;;
	-q) export MSG="$2 "
		run_path=$(dirname $(readlink $0))/query.sh
		xfce4-terminal --disable-server --geometry "$w"x"$h"+0-0 --hide-borders -x $run_path
		cat $ret
		echo > $ret ;;
	-p) run_path=$(dirname $(readlink $0))/psw.sh
		xfce4-terminal --disable-server --geometry 30x3+0-0 --hide-borders -x $run_path
		cat $ret
		echo > $ret ;;
	*) echo "Usage:
  -m MESSAGE DELAY(seconds)
  -q QUERY
  -p PASSWORD" ;;
esac

